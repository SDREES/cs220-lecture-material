# Project 3 (P3) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don't lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)

### Question specific guidelines:

- Q1 deductions

  - `prius_id` variable is hard coded as 977 (-5)

- Q2 deductions

  - `num_leaf` variable is hard coded as 11230 (-5)
  - output of `get_id` function is not used as input to `get_sales` function (-3)

- Q3 deductions

  - `max_sales_2017` variable is hard coded as 26500 (-5)
  - `year_max` function is not used (-3)
  - conditionals/iterations are used to get output (-2)

- Q4 deductions

  - `max_sales_2016_to_2018` variable is hard coded as 43525 (-5)
  - `year_max` function is not used (-3)
  - max function is not used or conditionals/iterations are used to get output (-2)

- Q5 deductions

  - `min_sales_model_s` variable is hard coded as 10125 (-5)
  - `sales_min` function is not used (-3)
  - min function is not used in `sales_min` function or conditionals/iterations are used to get output (-2)

- Q6 deductions

  - `min_sales_CV_FFE_NL` variable is hard coded as 16 (-5)
  - `sales_min` function is not used (-3)
  - min function is not used or conditionals/iterations are used to get output (-2)

- Q7 deductions

  - `sales_avg_prius_2017_to_2021` variable is hard coded as 34939.2 (-5)
  - `sales_avg` function is not used or conditionals/iterations are used to get output (-3)
  - `get_id` function is not used in `sales_avg` function (-1)
  - `get_sales` function is not used in `sales_avg` function (-1)

- Q8 deductions

  - `sales_avg_volt_2017_to_2021` variable is hard coded as 8730.6 (-5)
  - `sales_avg` function is not used or conditionals/iterations are used to get output (-3)

- Q9 deductions

  - `diff_leaf_2018_to_average` variable is hard coded as 2292.3999999999996 (-5)
  - `sales_avg` function is not used or conditionals/iterations are used to get output (-3)
  - `get_sales` function is not used for getting Nissan Leaf car sales (-1)

- Q10 deductions

  - `sales_sum_2021` variable is hard coded as 116806 (-5)
  - `year_sum` function is not used (-3)
  - Passed more arguments than necessary to `year_sum` function (-1)
  - Default value for year=2021 is changed in `year_sum` function (-1)

- Q11 deductions

  - `sales_sum_2019_to_2021` variable is hard coded as 289765 (-5)
  - `year_sum` function is not used (-3)
  - Passed more arguments than necessary to `year_sum` function (-1)

- Q12 deductions

  - `fusion_average_change` variable is hard coded as -1572.5 (-5)
  - `change_per_year` function is not used (-3)
  - Passed more arguments than necessary to `change_per_year` function (-1)
  - Default values for `start_year`=2017, `end_year`=2021 are changed in `change_per_year` function (-1)
  - `get_id` function is not used in `change_per_year` function (-1)
  - `get_sales` function is not used in `change_per_year` function (-1)

- Q13 deductions

  - `volt_average_change` variable is hard coded as -6096.666666666667 (-5)
  - `change_per_year` function is not used (-3)
  - Passed more arguments than necessary to `change_per_year` function (-1)

- Q14 deductions

  - `model_x_average_change` variable is hard coded as 2389.0 (-5)
  - `change_per_year` function is not used (-3)
  - Passed more arguments than necessary to `change_per_year` function (-1)

- Q15 deductions

  - `leaf_sales_in_2025` variable is hard coded as 17248.0 (-5)
  - `estimate_sales` function is not used (-3)
  - `change_per_year` function is not used in `estimate_sales` function (-3)
  - Passed more arguments than necessary to `estimate_sales` function (-1)
  - Default values for `start_year`=2017 and `end_year`=2021 are not specified in `estimate_sales` function (-1)

- Q16 deductions

  - `prius_sales_in_2026` variable is hard coded as 91315.0 (-5)
  - `estimate_sales` function is not used (-3)
  - Required arguments are not passed to `estimate_sales` function (-1)

- Q17 deductions

  - `diff_sales_model_x_2030` variable is hard coded as -41541.5 (-5)
  - `estimate_sales` function is not used (-3)
  - Passed more arguments than necessary to `estimate_sales` function (-1)

- Q18 deductions

  - `diff_sales_leaf_2030` variable is hard coded as 32092.5 (-5)
  - `estimate_sales` function is not used (-3)
  - Passed more arguments than necessary to `estimate_sales` function (-1)

- Q19 deductions

  - `volt_diff_change_per_year` variable is hard coded as -406.5 (-5)
  - `change_per_year` function is not used (-3)
  - Passed more arguments than necessary to `change_per_year` function (-1)

- Q20 deductions
  - `prius_change_per_year_ratio` variable is hard coded as 2.090140253191154 (-5)
  - `change_per_year` function is not used (-3)
  - Passed more arguments than necessary to `change_per_year` function (-1)
